# Generated by Django 4.2.1 on 2023-06-06 08:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Transactions',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=25)),
                ('item', models.CharField(max_length=25)),
                ('quantity', models.IntegerField()),
                ('selling_price', models.IntegerField()),
                ('transactiondttm', models.DateTimeField()),
            ],
        ),
    ]
