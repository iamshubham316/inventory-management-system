# Generated by Django 4.2.1 on 2023-06-08 10:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0004_alter_inventory_profit_earned_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inventory',
            name='iin',
            field=models.CharField(blank=True, default='4fa0d32e', max_length=8, unique=True),
        ),
    ]
